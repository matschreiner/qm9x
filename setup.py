from setuptools import find_packages, setup

setup(
    author="Mathias Schreiner",
    author_email="matschreiner@gmail.com",
    name="qm9x",
    version="1.0.0",
    packages=find_packages(),
    install_requires=["h5py", "ase", "tqdm", "progressbar"],
    extras_require={'test': ['pytest']}
)
