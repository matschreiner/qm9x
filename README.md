#### Installation
To install, run:
```
$ git clone https://gitlab.com/matschreiner/qm9x
$ cd qm9x
$ pip install .
```
To download the qm9x.h5 to a directory (default is 'data/qm9x.h5'), run:

```
$ python download_qm9x.py {directory}
```

#### Usage

The elements in the data loader each represent a single molecule. It is a dictionary that has the following keys available:
*    formula:            chemical formula for the molecule.
*    positions:          list of x, y, z coordinates of all atoms in the molecule in Å.
*    atomic_numbers:     list of atomic numbers ordered in the same way as positions.
*    energy:             total energy of molecule in eV.
*    atomization_energy: atomization energy of molecule in eV.
*    forces:             list of x, y, z forces on each atom in eV/Å - atoms are ordered in the same way as in positions.


To use the dataloader, run the following python code

```
from qm9x import Dataloader

dataloader = Dataloader(path_to_h5_file)
for molecule in dataloader:
    energy = molecule["energy"]  # Access the energy of all molecules in the dataloader
    ...
```


#### Examples

##### scripts/write_asedb.py
In this example we generate an ase.db database where each row has energy, forces and atomization\_energy in the data-field. By default we read the h5file from data/qm9x.h5 and write to data/qm9x.db

```
python scripts/write_asedb.py {path_hdf5_in} {path_db_out}
```

##### scripts/simple.py
This example loops through the first 20 configurations in the dataset and prints them as pretty dicts.
