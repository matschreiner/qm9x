import json
import os
from argparse import ArgumentParser

import ase.db
from ase import Atoms
from qm9x import Dataloader
from tqdm import tqdm


def main(args):  # pylint: disable=redefined-outer-name
    assert not os.path.exists(args.db), f"{args.db} exists already"
    os.makedirs(os.path.dirname(args.db), exist_ok=True)
    dataloader = Dataloader(args.h5)

    with ase.db.connect(args.db) as db:
        for configuration in tqdm(dataloader):
            atoms = Atoms(configuration["atomic_numbers"])
            atoms.set_positions(configuration["positions"])

            data = {
                "energy": configuration["atomization_energy" ],
                "forces": configuration["forces"],
            }

            db.write(atoms, data=data)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("h5", nargs="?", default="data/qm9x.h5")
    parser.add_argument("db", nargs="?", default="data/qm9x.db")
    args = parser.parse_args()

    main(args)
