from argparse import ArgumentParser
from pprint import pprint

from qm9x import Dataloader


def main(args):  # pylint: disable=redefined-outer-name
    # loop through all configurations in the data set
    dataloader = Dataloader(args.h5file)
    for i, mol in enumerate(dataloader):
        formula = mol.pop('formula')
        print (f"##### {formula} #####\n")
        pprint(mol)
        print(2*'\n')
        if i > 20:
            break


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("h5file", nargs="?", default="data/qm9x.h5")
    args = parser.parse_args()

    main(args)
